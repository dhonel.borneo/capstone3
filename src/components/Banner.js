import {Button, Container, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import {useContext} from 'react';
import UserContext from '../UserContext.js';

export default function Banner(){
	const {user} = useContext(UserContext);
	
	return(
		<Container>
			<Row>
				<Col className = 'mt-3 text-center'>
					{
						user.isAdmin !== true
						?
						<>
							<h1>Mermaid Collections by Caryl</h1>
							<p>Diving gears and more</p>
							<Button as = {Link} to = '/products'>Shop Now!</Button>
						</>
						:
						<>
							<h1>Welcome Admin!</h1>
							<p>I hope you are doing great!</p>
							<Button as = {Link} to = '/adminDashboard'>Manage Product!</Button>
						</>
					}
					
				</Col>
			</Row>
		</Container>
		);
}