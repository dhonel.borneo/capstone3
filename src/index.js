import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Bootsramp import
import 'bootstrap/dist/css/bootstrap.min.css';

// This is where React attaches the component to the root element in the index.html.
// Strictmode allowas react to be able to be display and handle anything
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

